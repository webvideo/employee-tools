// ==UserScript==
// @name         Auth for Scrile websites
// @namespace    http://tampermonkey.net/
// @version      0.1.4
// @description  Automatically substitute password
// @author       Backend
// @match      https://*/admin
// @match      https://*/admin/login
// @match      https://*/mielpotadmin1221
// @match      https://*/mielpotadmin1221/login
//
// @icon       data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @downloadURL https://gitlab.com/webvideo/employee-tools/-/raw/master/userscripts/scrile-auth.js
// @updateURL https://gitlab.com/webvideo/employee-tools/-/raw/master/userscripts/scrile-auth.js
// @grant      GM_xmlhttpRequest
// @grant      GM_openInTab
// @run-at document-idle
// @connect auth.dev.scrile.com
// ==/UserScript==

// cat .production-gitlab-ci.yml | grep '(?<=DOMAIN_NAMES: ").+(?=")(?<!connect.scrile.com)' -oP | sed 's/www.//g' | tr ' ' "\n" | xargs -i echo -e '// @match      https://*.{}/admin/login\n// @match      https://*.{}/admin'

(function() {
  'use strict';
  console.log('Substituting password');
  var inputCheckTimeout = 100;
  var authOpened = false;
  const startTime = new Date().getTime();
  const checkFunc = () => {
    if(new Date().getTime() > startTime + 30000) return;

    if (document.querySelectorAll("form").length === 1 && document.querySelector('input[name=email]') && document.querySelector('input[type=password]')) {
      GM_xmlhttpRequest({'method': 'get', 'url': 'https://auth.dev.scrile.com/code', 'onerror': () => {
          if(!authOpened) {
            GM_openInTab('https://auth.dev.scrile.com', {incognito: false});
            authOpened = true;
          }
          window.setTimeout(checkFunc, inputCheckTimeout);
        }, 'onload': (data) => {
          try {
            document.querySelector('input[name=email]').value = 'admin@scrile.com';
            document.querySelector('input[name=email]').dispatchEvent(new Event('input', {bubbles:true}));
            document.querySelector('input[type=password]').value = JSON.parse(data.responseText).password;
            document.querySelector('input[type=password]').dispatchEvent(new Event('input', {bubbles:true}));

            // Admin show error when we login too quickly, so we do it in timeout
            setTimeout(() => {
              const error = document.querySelector('header.status div.text__container div.text');
              if(error == null) {
                document.querySelector('button[type=button].primary').click();
              }
            }, 1000);
          } catch (e) {
            console.error(e);
          }
        }});
    } else {
      window.setTimeout(checkFunc, inputCheckTimeout);
    }
  }
  checkFunc();
})();
