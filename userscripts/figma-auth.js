// ==UserScript==
// @name         Auth for Figma
// @namespace    http://tampermonkey.net/
// @version      0.1.4
// @description  Automatically substitute password
// @author       Oleg Kolychev
// @match      https://www.figma.com/*
//
// @icon       data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @downloadURL https://gitlab.com/webvideo/employee-tools/-/raw/master/userscripts/figma-auth.js
// @updateURL https://gitlab.com/webvideo/employee-tools/-/raw/master/userscripts/figma-auth.js
// @grant      GM_xmlhttpRequest
// @grant      GM_openInTab
// @run-at document-idle
// @connect    google.com
// @connect    googleusercontent.com
// ==/UserScript==

(function() {
  'use strict';
  var inputCheckTimeout = 1100;
  var checkFunc = () => {
    if (document.location.href.startsWith("https://www.figma.com/") && document.evaluate("//div[text()='You need a password to access the file']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue != null) {
      GM_xmlhttpRequest({'method': 'get', 'url': 'https://docs.google.com/spreadsheets/d/1CpbCnftwOdYSWk0miqEu2bim3ly3yxwNjxobjNPM0Dc/export?format=csv', 'onerror': () => {
          GM_openInTab('https://docs.google.com/spreadsheets/d/1CpbCnftwOdYSWk0miqEu2bim3ly3yxwNjxobjNPM0Dc', {incognito: false});
        }, 'onload': (data) => {
          try {
            var name = document.querySelector("div[class^=link_password_input_app--fileName-]").innerText;
            var password = data.responseText.split("\n")
              .map(line => line.split(",", 2))
              .filter(vals => vals[0].trim().toLowerCase() == name.trim().toLowerCase())
              .map(vals => vals[1]);
            if(!password) {
              GM_openInTab('https://docs.google.com/spreadsheets/d/1CpbCnftwOdYSWk0miqEu2bim3ly3yxwNjxobjNPM0Dc', {incognito: false});
              return;
            }

            document.querySelector('input[name=password]').value = password;
            document.querySelector('input[name=password]').dispatchEvent(new Event('input', {bubbles:true}));
            document.querySelector('button[type=submit]').click();
          } catch(e) { console.error(e); }
        }});
    } else {
      window.setTimeout(checkFunc, inputCheckTimeout);
    }
  }
  checkFunc();
})();
